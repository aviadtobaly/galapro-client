import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { Socket } from 'ng-socket-io';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  data: Observable<any>;
  constructor(public navCtrl: NavController, public httpClient: HttpClient, public socket: Socket) {

    this.getURL().subscribe(data => {
      if(data.error) {
        this.errorNoRedir = true;
      }
      else {
        this.clURL = data.url;
      }
    });
  }

  getURL() {
    let observable = new Observable(observer => {
      this.socket.on('url', (data) => {
        observer.next(data);
      });
    });
    return observable;
  }

  submitClick() {
    this.errorNoRedir = false;
    this.errorNoURL = false;

    if (this.clURL && this.clURL.length > 0) {
      if (this.clRedirect) {

        // Part 1
          //
          /*this.data = this.httpClient.get('http://localhost:3000/api/redirect?url=' + this.clURL);
          this.data
            .subscribe(data => {
             if(data.url) {
               this.clURL = data.url;
             }
             else {
               this.errorNoRedir = true;
             }
            })*/



          // Part 2
          this.socket.connect();
          this.socket.emit('redirect', this.clURL);
      }

      else {
        // Redirect if no 'checkbox' selected
        window.open('http://www.google.com','_system')
      }
    }

    else {
      this.errorNoURL = true;
    }
  }


}
